import gUserInfo from './info'
import img from './assets/images/avatardefault_92824.png'

function App() {
  return (
    <div className="App">
      <h5>{gUserInfo.lastname} {gUserInfo.firstname}</h5>
      <img src={img} width='300px'/>
      <p>{gUserInfo.age}</p>
      <p>{gUserInfo.age <= 35 ? 'Anh ấy còn trẻ':'Anh ấy đã già'}</p>
      <ul>
        {
          gUserInfo.language.map((value,index)=>{
            return <li key={index}>{value}</li>
          })
        }
      </ul>
    </div>
  );
}

export default App;
